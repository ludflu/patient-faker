#!/usr/bin/env python

import random
import numpy as np
import csv

def random_line(fname):  #resevoir sampling https://stackoverflow.com/a/3540315/104887
    lines = iter(open(fname).read().splitlines())
    line = next(lines)
    for num, aline in enumerate(lines, 2):
      if random.randrange(num): continue
      line = aline
    return line.rstrip()

def make_social():
    a = random.randint(120,160)
    b = random.randint(40,88)
    c = random.randint(3123,9000)
    return "{}-{}-{}".format(a,b,c)

def make_ethnicity():  #data from 2010 census
    eths = [ 'White', 'African American', 'Asian', 'Native American', 'Native Hawaiian & Pacific Islander', 'Two or more', 'Other']
    probs = [ .724, .126, .048, .009, .002, .029, .062] #must sum to 1. Somehow Hispanic/Latino is 16.3 %, but not included as a category
    draw = np.random.choice( eths, 1, p = probs)  
    return draw[0]

with open('patients.csv', 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)

    for i in range(0,1000):
        first = random_line('data/first.txt')
        last = random_line('data/last.txt')
        ethnicity = make_ethnicity()
        social = make_social()
        age = abs(np.random.normal(37,20)) #years
        height = abs(np.random.normal(176,10)) #cm
        test = random.choice([True,False])
        writer.writerow( [first, last, ethnicity, social, str(age), str(height), test])
