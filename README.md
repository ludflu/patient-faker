# patient-faker
generates fake patient data suitable for use in demonstrating data privacy / anonymization techniques

## De-identification and Pseudonymization

The first step in anonymizing data is to remove identifiers like names and social security numbers.
You have some options here:

 - just remove the identifier fields. This is the safest choice, but also makes your data less useful because its now difficult to calculate aggregate statistics by individual. (This is called de-identification)
 - replace the identifiers with a GUID, and keep a private mapping table. This preserves the ability to calculate statistics by individual but makes re-identification harder (Pseudonymization (1))
 - replace identifiers with a one-way hash with salt. This preserves the ability to calculate statistics, and preserves identifiers, but does leak some information. Note that salting is required to prevent rainbow table attacks. (Pseudonymization (2))

Unfortunately, many attempts at making a dataset anonymous stop at this first step. De-identified data IS NOT anonymous because it leaves "quasi-identifiers" untouched.
Quasi-identifiers are attributes of an individual that can be used in combination to uniquely identify a person. For example, you might not know my
name and social security number, but you could undertake a "background information" re-identification attack if you found out my :

 - age
 - gender
 - ethnicity
 - height 
 - place of residence

You might be able to glean these details from public datasources. You would have a good chance of narrowing down which records belong to me. 
For this reason we need to go further if we want to make a dataset truly anonymous.

## k-anonymity

[k-anonymity](https://dataprivacylab.org/dataprivacy/projects/kanonymity/paper3.pdf) limits the effectiveness of re-identification attacks by ensuring that any combination of quasi-identifiers will result in at least k records.
You transform your data to meet the desired k by limiting the granularity of your data:

### Convert quantitative data to categorical

 - instead of storing age or date of birth, provide broad age buckets like 0-5, 6-15, 16-21, 22-35, 36-50, 50+ 
 - make sure that each age range contains enough people to support your desired k
 - it may be necessary to combine or cap low frequency values. (there's not that many people over the age of 90, and some zip codes have very few people.)
 - If you're representing locations you can impose a grid on the space and substitute lat/long coordinates with grid coordinates. 
 
### What if you don't have enough data to support the desired k?

Reducing the granularity of your data does make it less useful, and you might not have enough of it to reach the desired level of anonymity.
Another option is to introduce some fake data to keep the real data company. Obviously, this will also make your data less useful because anything calculated from it will be skewed by the fake data. For that reason, try using gaussian noise so that its net effect will cancel out.  

## [l-diversity](https://ieeexplore.ieee.org/abstract/document/1617392)

Even with all that, the privacy of your subjects is still not safe. 
If you have sensitive values in your data (such as test results), you need those values to be well-represented within each k-group.  Otherwise your dataset is susceptible to the homogeneity attack.

For example, if you limit your search to people of Native Hawaiian & Pacific Islander heritage, you will find that the quasi-identifiers for age and height have
been appropriately reduced in granularity so that the two individuals are indistinguishable. HOWEVER - they both happened to receive a TestResult of "True". So simply
by knowing that a person is in a dataset and what their ethnicity is, you can learn their sensitive test results. So to qualify as truly anonymous, under the l-diversity test,
each of the possible distinct sensitive values much be represented, which would make it harder to discover private information.

## [Differential Privacy](https://pdfs.semanticscholar.org/0a36/6f94baa5c64e26251ff059b91bae00cc0d29.pdf)

Researchers have found fear that even l-diversity is not strong enough, and that with enough queries, sub-sampling plus joining to publically available datasets, its still possible to
re-identify individuals. For privacy researchers who want the strongest guarantees and the highest standard of anonymity, there's Differential Privacy. Although I've not seen such systems in production, I'm told Apple and Google have deployed differential privacy systems, albeit with the weakest of guarantees. The basic idea is:

 - a given database is created and allocated a corresponding "privacy budget" that regulates the amount information that can be given out, as measured using information theoretic measures like entropy
 - each time a query is executed, the budget is depleted by an amount proportional to the information contained in the results returned.
 - when the budget is totally depleted, the database stops responding to queries.
